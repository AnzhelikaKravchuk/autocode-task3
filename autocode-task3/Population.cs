﻿using System;

namespace PadawansTask1
{
    public class Population
    {
        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            const int hundredPercents = 100;
            var numberOfYears = 0;
            
            double endOfYearPopulation = initialPopulation;

            if (initialPopulation <= 0)
                throw new ArgumentException($"{nameof(initialPopulation)} cannot be less or equals zero");

            if (percent < 0 || percent > hundredPercents)
                throw new ArgumentOutOfRangeException($"{nameof(percent)} cannot be less then 0% or more then 100%");

            if (visitors < 0)
                throw new ArgumentException($"{nameof(visitors)} cannot be negative.");

            if (currentPopulation <= initialPopulation)
                throw new ArgumentException($"{nameof(currentPopulation)} cannot be less or equal then {nameof(currentPopulation)}.");

            while (endOfYearPopulation < currentPopulation)
            {
                endOfYearPopulation = endOfYearPopulation * (hundredPercents + percent) / hundredPercents + visitors;
                numberOfYears++;
            }

            return numberOfYears;
        }
    }
}